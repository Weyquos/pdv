import sys
import time
import os

try:
    import RPi.GPIO as GPIO
except ImportError as ie:
    print("Problema ao importar modulo {0}").format(ie)
    sys.exit()


def print_msg(msg):
    tam = len(msg) + 4
    print('\n')
    print('#' * tam)
    print('# %s #' % msg)
    print('#' * tam)


def exit():
    GPIO.cleanup()


def info():
    print("Revisão Raspberry PI %d" % GPIO.RPI_REVISION)
    print("Versão RPi.GPIO %s" % GPIO.VERSION)

class keypad:
    __KEYPAD = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9],
        ["*", 0, "#"]
    ]

    __row = [38, 31, 33, 37]
    __column = [36, 40, 35]

    __last_key = None

    def __init__(self):
        self.check_user()
        # Iniciando o modo como BOARD e desabilitando os Warnings do Rpi.GPIO
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BOARD)

        # Configuro todos os pinos da coluna como OUT e nivel LOW(0V)
        for j in range(len(self.__column)):
            GPIO.setup(self.__column[j], GPIO.OUT)
            GPIO.output(self.__column[j], GPIO.LOW)

        # Configuro todos os pinos da linha como INPUT e Pull-Up
        for i in range(len(self.__row)):
            GPIO.setup(self.__row[i], GPIO.IN, pull_up_down=GPIO.PUD_UP)

        # Configurando as interrupcoes que iram ocorrer em algum dos pinos de linha
        GPIO.add_event_detect(self.__row[0], GPIO.FALLING, callback=self.trataPino, bouncetime=300)
        GPIO.add_event_detect(self.__row[1], GPIO.FALLING, callback=self.trataPino, bouncetime=300)
        GPIO.add_event_detect(self.__row[2], GPIO.FALLING, callback=self.trataPino, bouncetime=300)
        GPIO.add_event_detect(self.__row[3], GPIO.FALLING, callback=self.trataPino, bouncetime=300)

        self.valor_linha = -1
        self.valor_coluna = -1

    def check_user(self):
        if os.geteuid() != 0:
            print("Vocêeve executar o programa como super-usuario!")
            print("Exemplo:\nsudo python {0}").format(__file__)
            sys.exit()

    def get_key(self):

        self.valor_linha = -1
        self.valor_coluna = -1

        while self.valor_coluna == -1 and self.valor_linha == -1:
            pass

        self.__last_key = self.__KEYPAD[self.valor_linha][self.valor_coluna]

        # Retorna o valor da tecla pressionada baseada na linhaxcoluna
        return self.__KEYPAD[self.valor_linha][self.valor_coluna]

    def last_key(self):
        return self.__last_key

    def trataPino(self, pino):

        self.valor_linha = self.__row.index(pino)

        for i in range(len(self.__column)):

            GPIO.setup(self.__column[i], GPIO.IN)

            if GPIO.input(pino) == GPIO.HIGH:
                self.valor_coluna = i
                break

        self.restaura_gpio()

    def restaura_gpio(self):
        for i in range(len(self.__column)):
            GPIO.setup(self.__column[i], GPIO.OUT)
            GPIO.output(self.__column[i], GPIO.LOW)