import datetime
import json
import sqlite3
import threading
import urllib
import os

import cursor as cursor
import pymysql as pymysql
import pymysql.cursors
import requests
from flask import Flask, request, render_template, jsonify, json
import time
from Model.Produto import Produto

from pyfingerprint import PyFingerprint

app = Flask(__name__)

leitorStatus = False                    # Indica se o leitor biometrico esta conectado
f = None                                # Objeto do Leitor Biometrico
posicao = -1                            # indica a posição encontrada no leitor biometrico( -1 equivale a nenhuma digital encontrada para a amostra)
resultadoBio = 0
# Identifica se ocorreu uma tentantiva de acesso biometrico
sqlitePath = --

listaProdutos = []                      # Lista de produtos para exibição no painel
listaLocais = []
carrinho = []                           # Lista de intes no carrinho
usuario = 0                             # Usuario atualmente consumindo
emp = 213                               # Empreendimento do dispositivo(Valor inicial 213 = Republica)

# Atualiza o empreendimento de acordo com o registro local
try:
    connEmp = sqlite3.connect(sqlitePath)
    cursorEmp = connEmp.cursor()
    cursorEmp.execute("SELECT empreendimento FROM configuracao;")
    for rowEmp in cursorEmp.fetchall():
        emp = rowEmp[0]
    connEmp.close()
except Exception as err:
    print(err)

# ==================================Produção=================================#
iotUrl = --
ipbanco = --
userbanco = --
passbanco = --
db = --
charset = 'utf8mb4'
# ===========================================================================#

'''
Registra erros que ocorreram duratne a execução
'''
def registrarLogErro(erro):
    try:
        conn = sqlite3.connect(sqlitePath)
        cursor = conn.cursor()
        sql = "INSERT INTO log(erro, hora) VALUES (?,?)"
        cursor.execute(sql, (erro, datetime.datetime.now()))
        conn.commit()
        conn.close()
    except Exception as e:
        print(e)


'''
Função para conectar o leitor biometrico
Em caso de falha sera o valor de LeitorStatus para False

def conectarLeitor():
    global leitorStatus
    global f
    try:
        f = PyFingerprint('/dev/ttyS0', 57600, 0xFFFFFFFF, 0x00000000)
        f.generateRandomNumber()
        leitorStatus = True
    except Exception as e:
        registrarLogErro(str(e))
        leitorStatus = False
'''

'''
Atualiza as digitais do leitor local

def updateDigitais():
    global posicao
    cont = 0
    try:
        f.clearDatabase()
        resetPosicoesBio()
        usuarios = requests.get("http://alugueap.com/userpweb/api/rasp/get-digitais-usuarios.php")
        for user in json.loads(usuarios.text):
            idUsuario = int(user['Cod'])

            conn = sqlite3.connect(sqlitePath)
            cursorLocal = conn.cursor()
            cursorLocal.execute("INSERT OR IGNORE INTO usuarioPosicao VALUES(?,?,?)", (idUsuario, None, None))
            conn.commit()

            if user['d0'] is not None:
                f.uploadCharacteristics(0x02, eval(user['d0']))
                posicao = f.storeTemplate(cont, 0x02)
                cursorLocal.execute("Update usuarioPosicao SET pd0 = ? WHERE idUsuario = ?;", (posicao, idUsuario))
                conn.commit()
                cont = cont + 1
            if user['d1'] is not None:
                f.uploadCharacteristics(0x02, eval(user['d1']))
                posicao = f.storeTemplate(cont, 0x02)
                cursorLocal.execute("Update usuarioPosicao SET pd1 = ? WHERE idUsuario = ?;", (posicao, idUsuario))
                conn.commit()
                cont = cont + 1
            conn.close()
    except Exception as e:
        registrarLogErro(e)
'''

'''
Thread que gerencia o leitor
Ao identificar uma leitura bem sucedida, envia a posição da digital para identificarLeitura()

class leitor(object):

    def __init__(self, interval=1):
        self.interval = interval
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        global leitorStatus
        try:
            conectarLeitor()
            updateDigitais()
            while True:
                if leitorStatus:
                    while not f.readImage():                # Aguarda que o leitor biometrico realize uma leitura
                        pass
                    f.convertImage(0x01)                    # Converte a imagem para o primeiro slot# Converte a imagem para o primeiro slot
                    result = f.searchTemplate()             # Realiza a busca no armazenamento local do leitor
                    positionNumber = result[0]              # Retorna a posição encontrada para a amostra (-1 caso não exista)
                    identificarLeitura(positionNumber)      # Executa a função para identificar a leitura
                else:
                    conectarLeitor()
                time.sleep(self.interval)
        except Exception as e:
            registrarLogErro(str(e))
            leitorStatus = False
            self.run()


leitorDigital = leitor()
'''

'''
Renderiza a pagina inicial
'''
@app.route('/')
def index():
    return render_template('pdv.html', data=time.strftime("%d/%m/%Y"), hora=time.strftime("%H:%M"), conexao="Online", versao="1.0.0", device="YouDO-PDV")


'''
Verifica se existe conexão com a API
'''
@app.route('/_get_conexao')
def get_conexao():
    try:
        urllib.request.urlopen("http://api.youdobrasil.com.br", timeout=3)
        con = "Online"
    except urllib.error.URLError as e:
        print(e)
        con = "Offline"
    return jsonify(con)


'''
Reinicia o dispositivo
'''
@app.route('/reboot')
def reboot():
    os.system('sudo reboot')
    return jsonify(1)

'''
Seta o usuario consumindo para 0
'''
@app.route('/_sair')
def sair():
    global usuario
    usuario = 0
    return jsonify(0)


'''
Seta o codigo do usuario que esta comprando
'''
@app.route('/_set_user')
def setUser():
    global usuario
    cod = request.args.get('cod')
    usuario = cod
    return jsonify(0)

'''
Valida o pin informado.
Success -1 -> PIN correto, porem não esta vinculado ao Club
Success  0 -> PIN incorreto.
Success  1 -> PIN correto e vinculado ao Club.
'''
@app.route('/_validate_pin')
def validatePin():
    pin = request.args.get('pin')
    try:
        iot = requests.get(iotUrl + "?classe=IOT&metodo=ConsultarPinUsuarioByPin&atributo[pin]=" + str(pin))
        cod = json.loads(iot.text)['list']['idusuario']
        if cod is None or cod == 'null':
            result = {'success': 0, 'cod': cod}
        else:
            if checkClubYd(cod) == 0:
                result = {'success': -1, 'cod': cod}
            else:
                result = {'success': 1, 'cod': cod}
        return jsonify(result)
    except Exception as e:
        registrarLogErro(e)
        result = {'success': 0, 'cod': None}
        return jsonify(result)


'''
Verifica se o usuario possui vinculo no YouDO Club.
'''
def checkClubYd(cod):
    try:
        user = int(requests.get("http://alugueap.com/userpweb/api/rasp/verificar-cadastro-youdo-club.php?idUsuario="+str(cod)).text)
        return user
    except Exception as e:
        registrarLogErro(e)
        return 0


'''
Retorna dados do usuario: Nome; Saldo; Créditos Temporarios
'''
@app.route('/_get_usuario')
def get_usuario():
    cod = request.args.get('cod')
    try:
        dados = requests.get("http://alugueap.com/userpweb/api/rasp/get-dados-usuario-club.php?idUsuario="+str(cod))
        if json.loads(dados.text)['Nome'] == "Null":
            user = {'Nome': 'Null', 'Credito': 0}
            return jsonify(user)
        user = {'Nome': json.loads(dados.text)['Nome'], 'Credito': float(json.loads(dados.text)['Credito']) + float(json.loads(dados.text)['CreditoT'])}
        return jsonify(user)
    except Exception as e:
        registrarLogErro(e)
        user = {'Nome': 'Null', 'Credito': 0}
        return jsonify(user)

'''
Seleciona o empreendimento do dispositivo
'''
@app.route('/_get_emp')
def getEmpreendimento():
    global emp
    try:
        conn = sqlite3.connect(sqlitePath)
        cursor = conn.cursor()
        cursor.execute("SELECT empreendimento FROM configuracao;")
        for row in cursor.fetchall():
            emp = row[0]
        conn.close()
    except Exception as e:
        registrarLogErro(e)
    return jsonify(emp)


'''
Retorna lista de dados do usuario: IdMembro, Tipo de Usuario,
'''
def get_dados_usuario(cod):
    try:
        dados = requests.get("http://alugueap.com/userpweb/api/rasp/get-dados-usuario-club.php?idUsuario="+str(cod))
        return json.loads(dados.text)['Hora'],  json.loads(dados.text)['Ydc'], json.loads(dados.text)['Tipo'], json.loads(dados.text)['pin'], json.loads(dados.text)['Credito'], json.loads(dados.text)['CreditoT']
    except Exception as e:
        registrarLogErro(e)
        return 0


'''
Utiliza a base local para retornar a lista de produtos disponiveis
'''
@app.route('/_get_produtos')
def getProdutos():
    p = []
    conn = sqlite3.connect(sqlitePath)
    cursorLocal = conn.cursor()
    cursorLocal.execute("SELECT cod, nome, barra, estoque, valor FROM produto")
    for row in cursorLocal.fetchall():
        inCart = checkItensInCart(row[0])
        p.append({'codigo': row[0], 'Nome': row[1], 'Barra': row[2], 'Estoque': row[3], 'Valor': row[4], 'Cart': inCart})
    conn.close()
    return jsonify(p)

'''
Atualiza a base local de produtos
'''
@app.route('/_update_produtos')
def updateProdutos():
    global listaProdutos
    global emp
    try:
        produtos = requests.get("http://alugueap.com/userpweb/api/rasp/produtosPDV.php?Empreendimento="+str(emp))
        listaProdutos.clear()
        for produto in json.loads(produtos.text):
            listaProdutos.append(Produto(int(produto['Cod']), str(produto['Nome']), str(produto['Barra']), int(produto['Estoque']), float(produto['Valor'])))
    except Exception as e:
        registrarLogErro(e)
    updateEstoqueLocal()
    return jsonify(1)

@app.route('/_get_locais')
def getLocais():
    l = []
    conn = sqlite3.connect(sqlitePath)
    cursorLocal = conn.cursor()
    cursorLocal.execute("SELECT local_id, local_nome FROM sigv_tb_localizacao")
    for row in cursorLocal.fetchall():
        l.append({'local_id': row[0], 'local_nome': row[1]})
    conn.close()
    return jsonify(l)

@app.route('/_update_locais')
def updateLocais():
    global emp
    try:
        conn = sqlite3.connect(sqlitePath)
        cursor = conn.cursor()
        cursor.execute("DELETE FROM sigv_tb_localizacao;")
        localizacoes = requests.get("http://alugueap.com/userpweb/api/rasp/get-localizacao.php?Empreendimento=" + str(emp))
        conn.commit()

        for l in json.loads(localizacoes.text):
            sql = "INSERT or REPLACE INTO sigv_tb_localizacao(local_id, local_nome) VALUES (?,?)"
            cursor.execute(sql, (int(l['Cod']), str(l['Nome'])))
            conn.commit()
        conn.close()

    except Exception as e:
        registrarLogErro(e)
    return jsonify(1)


'''
Finaliza o processo de compra, verificando se o usuario possui créditos o suficiente
Return -1 -> Erro durante processo de conclusão de venda(Verificar tabela de log)
Return  0 -> Créditos insuficientes
Return  1 -> Compra Concluida com Sucesso
Return  2 -> Retorno que ocorria quando era necessaria uma segunda validação do usuario ao finalizar a compra e o pin informado era incorreto
'''
@app.route('/_finalizar')
def finalizar():
    global usuario
    global emp
    cod = request.args.get('cod')
    mesa = request.args.get('mesa')
    obs = request.args.get('obs')
    consumirTemporario = False
    try:
        if cod != usuario:
            return jsonify(2)
        itens, valor = calcularCarrinho()
        hora, ydcId, tipo, pin, cred, credT = get_dados_usuario(cod)
        venda = 1 + int(checkMaxVenda())

        if (float(cred) + float(credT)) < float(valor):                 # Verifica se o usuario possui créditos suficientes
            return jsonify(0)
        if float(valor) > float(credT) > 0.00:                          # Verifica a existencia de Crédito Temporario e seta o valor de consumirTemporario para True, indicado que serão utilizados na cobrança do consumo.
            newCred = float(cred) + float(credT) - float(valor)
            newCredTemp = 0.00
            consumirTemporario = True
        elif float(valor) <= float(credT):
            newCred = float(cred)
            newCredTemp = float(credT) - float(valor)
            consumirTemporario = True
        else:
            newCredTemp = 0.00
            newCred = float(cred) - float(valor)
        if SalvarVenda(valor, hora, ydcId, tipo, pin, venda, newCred, consumirTemporario, newCredTemp, mesa, obs) == 0:  # Verifica se foi possivel efetuar os registros referentes ao consumo.
            return jsonify(-1)
    except Exception as e:
        registrarLogErro(e)
        return jsonify(0)
    return jsonify(1)


'''
Salva os dados do consumo no banco e envia o email par ao usuario
Return 0 -> Ocorreu um erro
Return 1 -> Dados registrados com sucesso
'''
def SalvarVenda(valor, hora, ydcId, tipo, pin, venda, newCred, consumirTemporario, newCredTemp, mesa, obs):

    global emp
    if emp == 213:
        mesa = 0
    cnx = None
    cursor = None
    try:
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset, cursorclass=pymysql.cursors.DictCursor)
        with cnx.cursor() as cursor:
            cursor.execute("INSERT INTO sigv_tb_venda (datavenda, cliente, hospede, tipousuario, formapagto, condicao, diavencto, subtotal, desconto, valortotal, idvendendor, vlrentrada, pincode, status, encerrado_automatico, valor_pago, credito, id_membro, id_empreendimento, numero_mesa, observacao) "
                           "VALUES('"+str(hora)+"', 0, 0, '"+tipo+"', 0, 0, 0, "+str(valor)+", 0, "+str(valor)+", 540, 0, '"+pin+"', 3, 0, 0, 1, " + str(ydcId) + ", "+str(emp)+", '"+str(mesa)+"', '"+str(obs)+"');")
            for i in carrinho:
                cursor.execute("INSERT INTO sigv_tb_venda_produto(id_venda, id_produto, qtde, subtotal, desconto, valortotal, unitario) VALUES("+str(venda)+", "+str(i.idproduto)+", 1, "+str(i.valor)+", 0, "+str(i.valor)+", "+str(i.valor)+");")
            extrato = int(requests.get("http://alugueap.com/userpweb/api/rasp/registrar-extrato-consumo.php?valor="+str(valor)+"&emp="+str(emp)+"&ClubId="+str(ydcId)+"&venda="+str(venda)).text)

            if extrato == 0:         # Caso ocorra algum erro ao registrar o extrato, uma exceção é lançada, dando rollback no registro
                raise Exception('Erro ao registrar extrato')

            if consumirTemporario:   # Consome créditos temporarios caso existente
                cursor.execute("UPDATE sigv_tb_credito_consumacao SET credito_saldo = " + str(newCredTemp) + " WHERE id_membro = " + str(ydcId) + " and datacredito = curdate();")
        cnx.commit()
        requests.get("http://alugueap.com/userpweb/api/rasp/enviar-email-consumo.php?idMembro=" + str(ydcId) + "&venda=" + str(venda))
    except Exception as e:
        cnx.rollback()
        registrarLogErro(str(e))
        return 0
    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()
    return 1


'''
Verifica o ultimo id de venda registrado
Return 0 -> Erro ao consultar valor
'''
def checkMaxVenda():
    try:
        user = int(requests.get("http://alugueap.com/userpweb/api/rasp/verificar-maxid-venda.php").text)
        return user
    except Exception as e:
        registrarLogErro(e)
        return 0


'''
Verifica a quantidade de itens no carrinho
'''
def checkItensInCart(cod):
    global carrinho
    qtde = 0
    for i in carrinho:
        if int(i.idproduto) == int(cod):
            qtde += 1
    return qtde


'''
Atualiza estoque do produto no banco local(Desativado)
'''
def updateEstoqueLocal():
    global listaProdutos
    conn = sqlite3.connect(sqlitePath)
    cursor = conn.cursor()
    cursor.execute("DELETE FROM produto;")
    conn.commit()
    for p in listaProdutos:
        sql = "INSERT or REPLACE INTO produto(cod, nome, barra, estoque, valor) VALUES (?,?,?,?,?)"
        cursor.execute(sql, (p.idproduto, p.nome, p.barra, p.estoque, p.valor))
        conn.commit()
    conn.close()


'''
Esvazia o carrinho
'''
@app.route('/_empty_cart')
def empty_cart():
    global carrinho
    carrinho = []
    return jsonify(0)


'''
Remove um item do carrinho pelo codigo
'''
@app.route('/_remove_item')
def remove_item():
    global carrinho
    cod = request.args.get('cod')
    encontrado = False
    for i in carrinho:
        if i.idproduto == cod and not encontrado:
            carrinho.remove(i)
            encontrado = True
    itens, valor = calcularCarrinho()
    result = {'success': 1, 'item': itens, 'valor': valor}
    return jsonify(result)


'''
Retorna a lista com os produtos no carrinho com seus respectivos ids, nomes e valores
'''
@app.route('/_get_cart')
def get_cart():
    global carrinho
    cart = []
    for i in carrinho:
        cart.append({'codigo': i.idproduto, 'Nome': i.nome, 'Valor': float(i.valor)})
    return jsonify(cart)


'''
Adiciona um item ao carrinho
'''
@app.route('/_add_item_carrinho')
def add_item():
    global listaProdutos
    global carrinho
    nome = ''
    valorItem = 0
    barra = ''
    estoque = 0
    cod = request.args.get('cod')

    conn = sqlite3.connect(sqlitePath)
    cursorLocal = conn.cursor()
    cursorLocal.execute("SELECT nome, barra, estoque, valor FROM produto WHERE cod = ?;", (int(cod),))
    for row in cursorLocal.fetchall():
        nome = row[0]
        barra = row[1]
        estoque = row[2]
        valorItem = row[3]
    conn.close()

    carrinho.append(Produto(cod, nome, barra, estoque, valorItem))
    itens, valor = calcularCarrinho()
    result = {'success': 1, 'item': itens, 'valor': valor, 'nome': nome, 'valorItem': valorItem}
    return jsonify(result)


'''
Retorna a quantidade de itens no carrinho e o valor total
'''
def calcularCarrinho():
    global carrinho
    itens = 0
    valor = 0.00
    for i in carrinho:
        itens = itens + 1
        valor = valor + i.valor
    return itens, valor


'''
Realiza um busca do produto pelo Nome ou Código de barras
'''
@app.route('/_buscar_produto')
def buscarProduto():
    p = []
    text = request.args.get('text')
    conn = sqlite3.connect(sqlitePath)
    cursorLocal = conn.cursor()
    cursorLocal.execute("SELECT cod, nome, barra, estoque, valor FROM produto WHERE replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace( lower(nome), 'á','a'), 'ã','a'), 'â','a'), 'é','e'), 'ê','e'), 'í','i'),'ó','o') ,'õ','o') ,'ô','o'),'ú','u'), 'ç','c') LIKE ? or barra LIKE ? ORDER BY nome;", ('%'+text+'%', '%'+text+'%'))
    for row in cursorLocal.fetchall():
        inCart = checkItensInCart(row[0])
        p.append({'codigo': row[0], 'Nome': row[1], 'Barra': row[2], 'Estoque': row[3], 'Valor': row[4], 'Cart': inCart})
    conn.close()
    return jsonify(p)


'''
Realiza um busca do produto pelo Código ou Código de barras
'''
@app.route('/_get_item_by_cod')
def getItemByCod():
    codigo = 0
    nome = ''
    barra = ''
    estoque = 0
    valor = 0.00
    cod = request.args.get('cod')
    conn = sqlite3.connect(sqlitePath)
    cursorLocal = conn.cursor()
    cursorLocal.execute("SELECT cod, nome, barra, estoque, valor FROM produto WHERE cod = ? or barra = ?", (cod, cod))
    for row in cursorLocal.fetchall():
        codigo = row[0]
        nome = row[1]
        barra = row[2]
        estoque = row[3]
        valor = row[4]
    p = ({'codigo': codigo, 'Nome': nome, 'Barra': barra, 'Estoque': estoque, 'Valor': valor})
    conn.close()
    return jsonify(p)


'''
Seta resultadoBio = 1 caso uma tentativa de acesso biometrico tenha sido realiazda, setando a variavel posicao para o valor encontrado pelo leitor

def identificarLeitura(resultPosition):
    global posicao
    global resultadoBio
    if resultPosition == -1:
        resultadoBio = -1
    else:
        resultadoBio = 1
        posicao = resultPosition
'''


'''
Verifica se ocorreu uma tentativa de acesso via biometria

@app.route('/_verificar_entrada_biometria')
def verificarEntradaBiometrica():
    global resultadoBio
    global posicao

    if resultadoBio == 0:
        resultadoBio = 0
        return jsonify(0)
    elif resultadoBio == -1:
        resultadoBio = 0
        return jsonify(-1)
    elif resultadoBio == 1:
        cod, nome = identificarEntradaBio(posicao)
        resultadoBio = 0
        if checkClubYd(cod) == 0:
            resultado = {'success': -2, 'cod': cod, 'nome': nome}
        else:
            resultado = {'success': 1, 'cod': cod}

        return jsonify(resultado)
'''


'''
Reseta os vinculos de posição x usuario no leitor biometrico

def resetPosicoesBio():
    conn = sqlite3.connect(sqlitePath)
    cursor = conn.cursor()
    sql = "UPDATE usuarioPosicao SET pd0 = null, pd1 = null;"
    cursor.execute(sql)
    conn.commit()
    conn.close()
'''


'''
Identifica o usuario relacionado a posição encontrada pelo leitor biometrico

def identificarEntradaBio(position):
    cod = 0
    nome = ''
    conn = sqlite3.connect(sqlitePath)
    cursorLocal = conn.cursor()
    cursorLocal.execute("SELECT usu.id, usu.nome FROM usuario usu INNER JOIN usuarioPosicao posi on posi.idUsuario = usu.id WHERE posi.pd0 = ? or posi.pd1 = ?;", (position, position))
    for row in cursorLocal.fetchall():
        cod = row[0]
        nome = row[1]
    conn.close()
    return cod, nome
'''

if __name__ == '__main__':
    app.run()
