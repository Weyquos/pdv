import datetime
import sqlite3
import time

import requests
from flask import json

sqlitePath = "/home/youdo/YouDO_Dev/iotyoudo.db"
#sqlitePath = "/home/pi/iotyoudo.db"
#sqlitePath = "/home/fabiano/Files/Dev_YouDO/iotyoudo.db"
# ==================================Produção=================================#
iotUrl = 'http://api.youdobrasil.com.br'
# ===========================================================================#

# ================================Homologação================================#
# iotUrl = 'http://18.228.40.159/Api/'
# ===========================================================================#

# Retorna a lista de usuarios
def get_usuarios():
    try:
        usuarios = requests.get("http://alugueap.com/userpweb/api/rasp/usuarios-youdo-club.php")
        return json.loads(usuarios.text)
    except Exception as e:
        registrarLogErro(str(e))
        return 0

# Atualiza a lista de usuarios do banco interno
def set_usuarios():
    try:
        conn = sqlite3.connect(sqlitePath)
        cursor = conn.cursor()
        clientes = get_usuarios()

        if clientes != 0:
            cursor.executemany("INSERT OR REPLACE INTO usuario VALUES(:Cod,:Nome,:Pin,:d0,:d1,:idMembro,:StatusClub);", get_usuarios())
            conn.commit()
            conn.close()
    except Exception as e:
        registrarLogErro(str(e))

# Registra erros na execução de outras funções
def registrarLogErro(err):
    try:
        conn = sqlite3.connect(sqlitePath)
        cursor = conn.cursor()
        sql = "INSERT INTO log(erro, hora) VALUES (?,?)"
        cursor.execute(sql, (err, datetime.datetime.now()))
        conn.commit()
        conn.close()
    except Exception as e:
        print(e)


# Cria a estrutura do banco interno caso não exitam as tabelas
con = sqlite3.connect(sqlitePath)
c = con.cursor()
c.execute("CREATE TABLE IF NOT EXISTS usuario(id INTEGER PRIMARY KEY, nome TEXT, pin CHAR, d0 BLOB, d1 BLOB, clubId INTEGER, clubStatus INTEGER);")
c.execute("CREATE TABLE IF NOT EXISTS usuarioPosicao(idUsuario INTEGER PRIMARY KEY, pd0 INTEGER, pd1 INTEGER);")
c.execute("CREATE TABLE IF NOT EXISTS configuracao(ip CHAR, userBanco CHAR, passBanco CHAR, database CHAR, charset CHAR, empreendimento INTEGER);")
c.execute("CREATE TABLE IF NOT EXISTS log(id INTEGER PRIMARY KEY AUTOINCREMENT, hora DATETIME,erro TEXT)")
c.execute("CREATE TABLE IF NOT EXISTS produto(cod INTEGER PRIMARY KEY, nome TEXT, barra TEXT, estoque INTEGER, valor REAL);")
c.execute("CREATE TABLE IF NOT EXISTS sigv_tb_localizacao(local_id INTEGER PRIMARY KEY, local_nome TEXT);")
con.close()

# Mantem o banco interno atualizado de 5 em 5 minutos
while True:
    try:
        set_usuarios()
    except Exception as erro:
        registrarLogErro(str(erro))
    time.sleep(300)
