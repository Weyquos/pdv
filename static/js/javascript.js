var pin = new Array();
var user = 0;           // Usuario consumindo atualmente
var modo = 0;           // 0: Login | 1:Concluir
var etapaDIgital = 0;   // 0: Login | 1:Concluir | 2:Em compra

//================================= Login Start =================================//

// Recebe os valores do teclado na tela de login
function calc(valor){
    var j = pin.length;
    if(valor == -1 && j > 0){
        pin.pop();
        $(".visor_valor").val(pin.join(""));
    }else if(j < 4){
        pin.push(valor);
        $(".visor_valor").val(pin.join(""));
         if(pin.length == 4){
            validarPin(pin.join("").toString());
            resetarTeclado();
         }
    }
}

function resetarTeclado(){
    pin = [];
    modo = 0;
    $('.visor_valor').val('');
}

function resetarTecladoObs(){
    pin = [];
    modo = 0;
    $('.visor_valor').val('');
}

// Valida o pin informado para login
function validarPin(pin){
    var modal = document.getElementById('ModalLogin');
    var msgErro = document.getElementById('ModalMensagem');
    $("#loadMe").modal({backdrop: "static",keyboard: false,show: true});
    $.getJSON('/_validate_pin', {pin:pin}, function(data) {
        if(data['success'] == 1){
            $.getJSON('/_update_produtos',  function(data) {
            });
            $.getJSON('/_update_locais',  function(data) {
            });
            carregarProdutos();
            carregarLocais();
            carregarUsuario(data['cod']);
            exibirInputCod();
            modal.style.display = "none";
            etapaDIgital = 2;
            setUser(data['cod']);
            setTimeout(function() {$("#loadMe").modal("hide");}, 10);
        }else if(data['success'] == -1){
            setTimeout(function() {$("#loadMe").modal("hide");}, 10);
            erro('Você não esta cadastrado no YouDO Club!');
        }else{
        setTimeout(function() {$("#loadMe").modal("hide");}, 10);
            erro('Pin Incorreto!');
        }
    });
    setTimeout(function() {$("#loadMe").modal("hide");}, 1000);
}

// Encerra o processo de consumo, esvaziando o carrinho e retornando para a tela de login
function sair(){
    var modal = document.getElementById('ModalLogin');
    EsvaziarCarrinho();
    modal.style.display = "block";
    etapaDIgital = 0;
    document.getElementById('mesa').innerHTML = '';
    document.getElementById('CodValue').value = "";
    $('#pNome').html("");
    $('#pCredito').html("");
    $.getJSON('/_sair', function(data) {
        user = 0;
    });
    $.getJSON('/_update_produtos',  function(data) {

    });
}

// Seta o usuario atualmente realizando o consumo
function setUser(cod){
    $.getJSON('/_set_user', {cod:cod}, function(data){
        user = cod;
    });
}

// Verifica se ocorreu uma tentativa de entrada biometrica
/*
function verificarEntradaBiometria(){
    var modal = document.getElementById('ModalLogin');
    $.getJSON('/_verificar_entrada_biometria',  function(data) {
        if(data == -1){
            erro("Digital Invalida!");
        }else if(data['success'] == -2){
            erro(data["nome"] + " você não esta cadastrado no YouDO Club!!");
        }else if(data['success'] == 1){
            if(etapaDIgital == 0){
                carregarProdutos();
                carregarUsuario(data['cod']);
                exibirInputCod();
                modal.style.display = "none";
                etapaDIgital = 2;
                setUser(data['cod']);
            }else if(etapaDIgital == 1){
                var idusuario = data['cod'];
                $.getJSON('/_get_usuario',  {cod:idusuario},function(data) {
                    creditos = parseInt(data['Credito']);
                    var mesa = document.getElementById('mesa').innerHTML;
                    var obs = document.getElementById('observacao').value;
                    $.getJSON('/_finalizar',  {cod:idusuario, mesa:mesa, obs:obs},function(data) {
                        if(data == -1){
                           erro("Não foi possível concluir o consumo!");
                        }else if(data == 0){
                           erro("Creditos Insuficientes!");
                        }else if(data == 2){
                           erro("Pin incorreto");
                        }else if(data == 1){
                            sucesso('Consumo realizado com sucesso!');
                            sleep(5000);
                            var modal = document.getElementById('ModalLogin');
                            EsvaziarCarrinho();
                            modal.style.display = "block";
                            resetarTeclado();
                            etapaDIgital = 0;
                            document.getElementById('mesa').innerHTML = '';
                            document.getElementById('observacao').innerHTML = '';
                            $.getJSON('/_sair', function(data) {
                                document.getElementById('CodValue').value = "";
                            });
                            $.getJSON('/_update_produtos',  function(data) {
                            });
                        }
                    });
                });
            }
        }
    });
}*/

// Carrega os dados do usuario para o painel
function carregarUsuario(cod){
    $.getJSON('/_get_usuario',  {cod:cod},function(data) {
        $('#pNome').html(data['Nome']);
        $('#pCredito').html('Saldo: ' +Number(data['Credito']).toFixed(2) + ' YC');
    });
}

//================================= Login END =================================//


//================================= Concluir Start =================================//

// Recebe valores do teclado da seleção de mesa
function mesa(valor){
    var table = document.getElementById('mesa').innerHTML;
    if(valor == -1){
        table = table.substring(0, table.length - 1);
        $("#mesa").html(table);
    }else if(table.length < 2){
        $("#mesa").append(valor);
    }
}

// Fecha a modal de seleção de mesa, voltando para o painel de consumo
function CancelMesa(){
    var modal = document.getElementById('ModalMesa');
    modal.style.display = "none";
}

// Inicia o processo de finalização da compra
function FinalizarConsmo(){
    ConcluirCompra();
}

// Finaliza a compra. Caso seja necessario informar a mesa, exibe o modal
function ConcluirCompra(){
    var itens = document.getElementById('itens').innerHTML;
    if(itens == '0'){
        erro("Nenhum item encontrado no carrinho!");
    }else{
        $.getJSON('/_get_emp',  function(data) {
            if(data == 213){
                $("#loadMe").modal({backdrop: "static",keyboard: false,show: true});
                var mesa = 0;
                var obs = document.getElementById('observacao').value;
                $.getJSON('/_get_usuario',  {cod:user},function(data) {
                    creditos = parseInt(data['Credito']);
                    $.getJSON('/_finalizar',  {cod:user, mesa:mesa, obs:obs},function(data) {
                        if(data == -1){
                            setTimeout(function() {$("#loadMe").modal("hide");}, 100);
                            erro("Não foi possível concluir o consumo!");
                        }else if(data == 0){
                            setTimeout(function() {$("#loadMe").modal("hide");}, 100);
                           erro("Creditos Insuficientes!");
                        }else if(data == 2){
                            setTimeout(function() {$("#loadMe").modal("hide");}, 100);
                            erro("Pin incorreto");
                        }else if(data == 1){
                            sucesso("Consumo realizado com sucesso!");
                            var modal = document.getElementById('ModalLogin');
                            EsvaziarCarrinho();
                            setTimeout(function() {modal.style.display = "block";}, 4000);
                            resetarTeclado();
                            resetarTecladoObs();
                            etapaDIgital = 0;
                            document.getElementById('mesa').innerHTML = '';
                            document.getElementById('observacao').innerHTML = '';
                            $.getJSON('/_sair', function(data) {
                                document.getElementById('mesa').innerHTML = '';
                                document.getElementById('CodValue').value = "";
                            });
                            $.getJSON('/_update_produtos',  function(data) {
                            });
                            setTimeout(function() {$("#loadMe").modal("hide");}, 100);
                        }
                    });
                });

            }else{
                var modal = document.getElementById('ModalMesa');
                modal.style.display = "block";
            }
        });
    }
}

// Finaliza a compra após receber o numero da mesa
function ConcluirCompraComMesa(){
    var mesa = document.getElementById('mesa').innerHTML;
    var obs = document.getElementById('observacao').value;

    if(mesa == ""){
        erro("Informe o numero de sua mesa.");
    }else{
        var modalM  = document.getElementById('ModalMesa');
        var modalMT = document.getElementById('ModalTecladoObs');
        modalM.style.display = "none";
        var mesa = document.getElementById('mesa').innerHTML;
        var obs  = document.getElementById('observacao').value;
         $("#loadMe").modal({backdrop: "static",keyboard: false,show: true});
        $.getJSON('/_get_usuario',  {cod:user},function(data) {
            creditos = parseInt(data['Credito']);
            $.getJSON('/_finalizar',  {cod:user, mesa:mesa, obs:obs},function(data) {
                if(data == -1){
                    setTimeout(function() {$("#loadMe").modal("hide");}, 100);
                    erro("Não foi possível concluir o consumo!");
                }else if(data == 0){
                    setTimeout(function() {$("#loadMe").modal("hide");}, 100);
                   erro("Creditos Insuficientes!");
                }else if(data == 2){
                    setTimeout(function() {$("#loadMe").modal("hide");}, 100);
                    erro("Pin incorreto!");
                }else if(data == 1){
                    sucesso("Consumo realizado com sucesso!");
                    sleep(5000);
                    var modal = document.getElementById('ModalLogin');
                    EsvaziarCarrinho();
                    modal.style.display = "block";
                    resetarTeclado();
                    resetarTecladoObs();
                    etapaDIgital = 0;
                    document.getElementById('mesa').innerHTML = '';
                    document.getElementById('observacao').value = '';
                    $.getJSON('/_sair', function(data) {
                        document.getElementById('mesa').innerHTML = '';
                        document.getElementById('searchInput').value = "";
                        document.getElementById('obs').value = "";
                        document.getElementById('observacao').value = '';
                    });
                    $.getJSON('/_update_produtos',  function(data) {
                    });
                    setTimeout(function() {$("#loadMe").modal("hide");}, 100);
                }
            });
        });
    }
}

//================================= Concluir END =================================//


//================================= Produto Start =================================//

// Exibe a modal com detalhes do produto
function showModalProduto(codigo){
    var modal = document.getElementById('ModalProduto');

    $.getJSON('/_get_item_by_cod', {cod:codigo}, function(data) {
        if(data['codigo'] != 0){
            $('#NomeProd').html(data['Nome']);
            $('#ValorProd').html(Number(data['Valor']).toFixed(2) + ' YC');
            $('#btnAdicionarProd').html('<button class="btnModalProduto" onclick="addItemCod('+data['codigo']+')">Adicionar</button>');
            modal.style.display = 'block';
        }else{
            erro("Item Não Encontrado");
        }
    });

}

// Fecha a modal de detalhes  do produto
function closeModalProduto(){
    var modal = document.getElementById('ModalProduto');
    modal.style.display = 'none';
}

// Atualiza a lista de produtos com a base local
function carregarProdutos(){
    $.getJSON('/_get_produtos',  function(data) {
        atualizarListaProdutos(data);
    });
}


function carregarLocais(){
    $.getJSON('/_get_locais',  function(data) {
        atualizarListaLocais(data);
    });
}

//================================= Produto END =================================//


//================================= Carrinho Start =================================//

// Atualiza o carrinho
function atualizarCarrinho(){
    $.getJSON('/_get_cart',  function(data) {
        $("#tbCarrinho").find("tr").remove();
        for(var i = 0;i < data.length;i++){
            $("#tbCarrinho").append('<tr><td onclick="showModalProduto(' + data[i]["codigo"] + ')"><div style="width:180px;padding-top:7px;font-size:14px;font-weight:bold;">' + data[i]['Nome'] + '</div></td><td><div style="padding-top:7px;font-size:14px;">'+ Number(data[i]['Valor']).toFixed(2) + ' YC</div></td><td style="padding:1px;"><div style="padding-top:5px;"><button onclick="removeItem('+data[i]["codigo"]+')" class="btnRemove"><i style="color:white;" class="fas fa-trash-alt"></i></button></div></td><td style="padding:1px;"><div style="padding-top:5px;"><button onclick="buscarCodInputCart('+data[i]["codigo"]+')" class="btnAddCarrinho"><i style="color:white;" class="far fa-clone"></i></button></div></td></tr>');
        }
        var itens = document.getElementById('itens').innerHTML;
        if(itens == '0'){
            $( "#btnFinalizar" ).attr("class", "btnFinalizarOff");
        }else{
            $( "#btnFinalizar" ).attr("class", "btnFinalizar");
        }
    });
}

// Remove o item do carrinho com base no código
function removeItem(codigo){
    $.getJSON('/_remove_item', {cod:codigo}, function(data) {
        $('#itens').html(data['item']);
        $('#valor').html(Number(data['valor']).toFixed(2) + ' YC');
        $('#valorCarrinho').html('Total ' + Number(data['valor']).toFixed(2) + ' YC');
        atualizarCarrinho();
        carregarProdutos();
    });
}

// Adiciona(Replica) produto atravez do código
function buscarCodInputCart(codigo){
    $.getJSON('/_get_item_by_cod', {cod:codigo}, function(data) {
        if(data['codigo'] != 0){
            addItemCod(codigo);
        }else{
            erro("Item Não Encontrado");
        }
        document.getElementById('CodValue').value = "";
    });
}

// Remove todos os itens do carrinho
function EsvaziarCarrinho(){
    $.getJSON('/_empty_cart',  function(data) {
        $('#itens').html('0');
        $('#valor').html('0.00 YC');
        $('#valorCarrinho').html('Total 0.00 YC');
        carregarProdutos();
        atualizarCarrinho();
    });
}

//================================= Carrinho END =================================//


//================================= Lista de Protudos Start =================================//

// Altera a exibição do painel de consumo de Seleção por Código para Lista de Produtos
function exibirLista(){
    var btnLista = document.getElementById('btnListaProd');
    var btnCod = document.getElementById('btnCod');
    var btnBusca = document.getElementById('btnBusca');
    var lista = document.getElementById('ListaProdutos');
    var codInp = document.getElementById('CodInput');

    btnLista.style.display = 'none';
    codInp.style.display = 'none';
    btnCod.style.display = 'block';
    btnBusca.style.display = 'block';
    lista.style.display = 'block';
}



// Preenche a Lista de Produtos com a base local
function atualizarListaProdutos(data){
    $("#tableProd").find("tr").remove();
    for(var i = 0;i < data.length;i++){
         $('#tableProd').append('<tr class="trProd"><td onclick="showModalProduto(' + data[i]["codigo"] + ')"><div style="font-weight:bold;width:150px;padding-top:18px;font-size:14px;">' + data[i]['Nome'] + '</div></td><td><div style="padding-top:14px;font-size:14px;">' + Number(data[i]['Valor']).toFixed(2) + ' YC</div></td><td><button onclick="addItemCod('+data[i]["codigo"]+')" class="btnAdd"><i class="fas fa-cart-plus"></i></button></td></tr>');
    };
}

function atualizarListaLocais(data){
    $('#ListaLocalizacoes').css('display','block');
    $("#tableLocal").find("tr").remove();

    for(var i = 0;i < data.length;i++){
        $('#tableLocal').append('<tr class="trProd" onclick="gravarMesa(\'' + data[i]['local_nome'] + '\')"><td><div style="font-weight:bold;width:50px;padding-top:10px;font-size:14px;">' + data[i]["local_id"] + '</div></td><td><div style="padding-top:7px;font-size:14px;">' + data[i]['local_nome'] + ' </div></td></tr>');
    };
}

function gravarMesa(strMesa) {
    document.getElementById('mesa').innerHTML = strMesa;
}

// Adiciona o item com base no código
function addItemCod(codigo){
     $.getJSON('/_add_item_carrinho', {cod:codigo}, function(data) {
        if(data['success'] == 1){
            $('#itens').html(data['item']);
            $('#valor').html(Number(data['valor']).toFixed(2) + ' YC');
            $('#valorCarrinho').html('Total ' + Number(data['valor']).toFixed(2) + ' YC');
            atualizarCarrinho();
            carregarProdutos();
            closeModalProduto();
        }else{
            closeModalProduto();
            erro("Item esgotado!");
        }
    });
 }

//================================= Lista de Protudos END =================================//


//================================= Busca por Código Start =================================//

// Altera a exibição do painel de consumo de Lista de Produtos para Seleção por Código
function exibirInputCod(){
    var btnLista = document.getElementById('btnListaProd');
    var btnCod = document.getElementById('btnCod');
    var btnBusca = document.getElementById('btnBusca');
    var lista = document.getElementById('ListaProdutos');
    var codInp = document.getElementById('CodInput');

    btnCod.style.display = 'none';
    btnBusca.style.display = 'none';
    lista.style.display = 'none';
    btnLista.style.display = 'block';
    codInp.style.display = 'block';
}


// Recebe os valores informados no painel de consumo na Seleção por Código
function tecladoCodInput(valor){
    var text = document.getElementById('CodValue').value;
    text += valor;
    document.getElementById('CodValue').value = text;
}

// Apaga o ultimo valor do Input da Seleção por Código
function apagarCod(){
    var text = document.getElementById('CodValue').value;
    text = text.substring(0, text.length - 1);
    document.getElementById('CodValue').value = text;
}

// Busca pelo item, caso encontrado adiciona ao carrinho
function buscarCodInput(){
    var codigo = document.getElementById('CodValue').value;
    $.getJSON('/_get_item_by_cod', {cod:codigo}, function(data) {
        if(data['codigo'] != 0){
            addItemCod(codigo);
        }else{
            erro("Item Não Encontrado");
        }
        document.getElementById('CodValue').value = "";
    });
}

//================================= Busca por Código END =================================//

//================================= Teclado de Busca por Nome START =================================//
//Exibe teclado para realizar as observações
function exibirTecladoObs(){
    document.getElementById('obs').value = '';
    var modal = document.getElementById('ModalTecladoObs');
    modal.style.display = "block";
}

function fecharTecladoObs(){
    var modal = document.getElementById('ModalTecladoObs');
    modal.style.display = "none";
}

function tecladoInputObs(valor){
    var text = document.getElementById('obs').value;
    text += valor;
    document.getElementById('obs').value = text;
}

function anotar(){
    var text = document.getElementById('obs').value;
    document.getElementById('observacao').value = text;
    fecharTecladoObs();
}


// Exibe teclado para realizar busca por nome
function exibirTeclado(){
    document.getElementById('searchInput').value = '';
    var modal = document.getElementById('ModalTeclado');
    modal.style.display = "block";
}

// Fecha o teclado, retornando para o painel de consumo
function fecharTeclado(){
    var modal = document.getElementById('ModalTeclado');
    modal.style.display = "none";
}

// Recebe os valores do teclado de busca, adicionando ao input
function tecladoInput(valor){
    var text = document.getElementById('searchInput').value;
    text += valor;
    document.getElementById('searchInput').value = text;
}

// Apaga o ultimo valor inserido no campo de busca
function apagar(){
    var text = document.getElementById('searchInput').value;
    text = text.substring(0, text.length - 1);
    document.getElementById('searchInput').value = text;
}

function apagarObs(){
    var text = document.getElementById('obs').value;
    text = text.substring(0, text.length - 1);
    document.getElementById('obs').value = text;
}

// Realiza a busca de produto por nome
function buscar(){
    var text = document.getElementById('searchInput').value;
    var modal = document.getElementById('ModalTeclado');
    $.getJSON('/_buscar_produto', {text:text}, function(data) {
        atualizarListaProdutos(data);
        modal.style.display = "none";
    });
}

//================================= Teclado de Busca por Nome END   =================================//

//================================== Alerts START =================================//

// Exibe a mensagem de erro
function erro(msg){
    $('#msgErro').html(msg);
    $( "#errorAlert" ).fadeIn(1000);
    setTimeout(function() {$('#errorAlert').hide();}, 4000);
}

// Exibe mensagem de sucesso
function sucesso(msg){
    $('#msgSucesso').html(msg);
    $("#successAlert").fadeIn(500);
    setTimeout(function() {$('#successAlert').hide();}, 5000);
}

// Automaticamente sai da sessão de consumo caso o usuario fique ausente
function timerIncrement() {
    if(user != 0 && etapaDIgital != 0){
        idleTime = idleTime + 1;
        if (idleTime > 120) {
            $( "#idleAlert" ).hide();
            sair();
            user = 0;
        }

        // Exibe alerta de ociosidade 30 segundos antes do tempo limite
        if(idleTime > 90 && (120 - idleTime) > 0 && user != 0  && etapaDIgital != 0){
            $('#msgOciosidade').html("Você será desconectado por ociosidade em: " + (120 - idleTime) + " segundos");
            $( "#idleAlert" ).fadeIn(1000);
        }else{
            $( "#idleAlert" ).hide();
        }
    }else{
        $( "#idleAlert" ).hide();
    }
}

//================================= Alerts END =================================//

// Atualiza o horario
function relogio(){
    var dia = new Date();
    var h = dia.getHours();
    var m = dia.getMinutes();
    var dd = dia.getDate();
    var mm = dia.getMonth()+1;
    var yyyy = dia.getFullYear();
    if(dd<10) {
        dd = '0'+dd
    }
    if(mm<10) {
        mm = '0'+mm
    }
    today = dd + '/' + mm + '/' + yyyy;
    h = checkTime(h);
    m = checkTime(m);
    document.getElementById('hora').innerHTML = h + ":" + m;
    document.getElementById('data').innerHTML = today;
}

// Formata horario
function checkTime(i) {
    if (i < 10) {i = "0" + i};
    return i;
}

// Verifica se o dispositivo esta conectado
function checkConnection(){
    $.getJSON('/_get_conexao',  function(data) {
        document.getElementById('con').innerHTML = data.toString();
    });
}

function sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

$(document).ready(function() {
    //================================= Hide Alerts =================================//
    $('#errorAlert').hide();
    $('#successAlert').hide();
    $('#idleAlert').hide();
    //================================= Hide Alerts =================================//
    $.getJSON('/_update_produtos',  function(data) { });    // Atualiza a lista local de produtos
    $(this).mousemove(function (e) {idleTime = 0;});        // Reseta o timer de ociosidade caso ocorra movimento do cursor
    $(this).keypress(function (e) {idleTime = 0;});         // Reseta o timer de ociosidade caso alguma tecla seja pressionada
    var idleInterval = setInterval(timerIncrement, 1200);   // Verifica de 2 em 2 minuto se o usuario esta ativo
    //setInterval(verificarEntradaBiometria, 2000);           // Verifica se ocorreu uma entrada por biometria de 2 em 2 segundos
    setInterval(relogio, 1000);                             // Atualiza o relógio a cada segundo
    setInterval(checkConnection, 30000);                    // Verifica se o dispositivo esta conectado a cada 30 segundos
});
